<?php include("headertwo.php"); ?>

<div class="fullblock sliderblock">
<div class="blockwrap aboutimages">
<img src="images/Smart-D-services.jpg" align="absmiddle" class="firstimage">
</div>

</div>
<div class="blockwrap sliderbarblue">

<div class="blockwrap clear pwidthleft">
</div>

</div>


<div class="fullblock container">


<div class="fullblock">
<div class="blockwrap clear pheight">
<div class="left article">
<div id="contents">
<h1>Smart-D Services</h1>

<p>
Smart D services changes the way you think about IT solutions, gathering, implementing and enhancing your network infrastructure to it’s full potential, helping you unleash the power of true collaboration environments, empowering your IT tools,& increasing your company´s productivity exponentially.
</p>

<div class="clear relative" style="padding-top:10px;">
    <div class="serviceBox left mwidthright" alt="itCloud">
        <h6> IT/Cloud Consulting (DYNACOM ONE Approach)</h6>
        <img src="images/services/IT-Consulting.jpg" class="serviceBoxImg">
        <img src="images/cloudIcon.png" align="absmiddle" width="25" style="border-radius:5px; position:absolute; top:-7px; right:7px;">
    </div>
    <div class="serviceBox left mwidthright" alt="maintenance">
        <h6>Maintenance Contracts & Service Policies</h6>
        <img src="images/services/Maintenance-Contracts.jpg" class="serviceBoxImg">
        <img src="images/maintenanceIcon.png" align="absmiddle" width="25" style="border-radius:5px; position:absolute; top:-7px; right:7px;">
    </div>
    <div class="serviceBox left"  alt="network">
        <h6>Network assessment (LAN, WAN and WiFi)</h6>
        <img src="images/services/Network-Assessment.jpg" class="serviceBoxImg">
        <img src="images/networkIcon.png" align="absmiddle" width="25" style="border-radius:5px; position:absolute; top:-7px; right:7px;">
    </div>
    <div class="serviceBox left mwidthright" alt="equipment">
        <h6>IT Equipment configuration and installation</h6>
        <img src="images/services/Install-&-Config.jpg" class="serviceBoxImg">
        <img src="images/itIcon.png" align="absmiddle" width="25" style="border-radius:5px; position:absolute; top:-7px; right:7px;">
    </div>
    <div class="serviceBox left mwidthright" alt="helpdesk">
        <h6>Help Desk<br><br></h6>
        <img src="images/services/help-desk.jpg" class="serviceBoxImg">
        <img src="images/helpDeskIcon.png" align="absmiddle" width="25" style="border-radius:5px; position:absolute; top:-7px; right:7px;">
    </div>
    
    <!-- Contenidos de Servicios ------------------------------------------------------- -->
    <div class="absolute clear itCloud serviceDesc">
    <div class="clear">
    <h2><img src="images/cloudIcon.png" align="absmiddle" width="40" style="border-radius:5px;"> IT/Cloud Consulting (DYNACOM ONE Approach)</h2>
    <p><img src="images/services/IT-Consulting.jpg" class="left serviceimg">
    Our Team of specialized consulters will advise you in the latest technological trends and solutions, 
    they can assist you ascertain which technology is best for you, based on the world´s best practices and 
    case studies.</p>

    <p>When you interact with one of our highly skilled consulters, he/she will listen to your needs, 
    catering them as a valued member of your internal IT Staff. Remember we are here to help you; our ONE 
    Account Managers will lead you to the resolution of all your IT needs. </p>
    </div>
    <div class="clear">
    <button class="viewmore backBtnService">Back</button>
    </div>
    </div>
    
    <div class="absolute clear maintenance serviceDesc">
    <div class="clear">
    <h2><img src="images/maintenanceIcon.png" align="absmiddle" width="40" style="border-radius:5px;"> Maintenance Contracts & Service Policies</h2>
    <p><img src="images/services/Maintenance-Contracts.jpg" class="left serviceimg">We understand that you 
    require business continuity to lead your respective market, and this is why we developed our maintenance 
    contracts so you can rest easy and let us take care of all your IT equipment for you, ensuring optimal 
    functionality all the time. We have established the policy that is right for you.</p>
    </div>
    <div class="clear">
    <button class="viewmore backBtnService">Back</button>
    </div>
    </div>
    
    <div class="absolute clear network serviceDesc">
    <div class="clear">
    <h2><img src="images/networkIcon.png" align="absmiddle" width="40" style="border-radius:5px;"> Network assessment (LAN, WAN and WiFi)</h2>
    <p><img src="images/services/Network-Assessment.jpg" class="left serviceimg">Have you experienced 
    bottlenecks or slowdowns in your network and you don´t know why? We can offer a network assessment 
    for your system´s infrastructure to validate if is running smoothly or not, most of the time network 
    slowdown is due configuration errors that can be easily fixed or just wrong cabling/hotspot distribution.</p>
    </div>
    <div class="clear">
    <button class="viewmore backBtnService">Back</button>
    </div>
    </div>
    
    <div class="absolute clear equipment serviceDesc">
    <div class="clear">
    <h2><img src="images/itIcon.png" align="absmiddle" width="40" style="border-radius:5px;"> IT Equipment configuration and installation</h2>
    <p><img src="images/services/Install-&-Config.jpg" class="left serviceimg">Need help installing 
    or configuring your IT equipment, we can help you with that, our team of engineers are always ready 
    to help no matter the issue at hand, from simply setting up a mail server to installing an configuring 
    a blade server system we have the knowledge to assist you with every IT need.</p>
    </div>
    <div class="clear">
    <button class="viewmore backBtnService">Back</button>
    </div>
    </div>
    
    <div class="absolute clear helpdesk serviceDesc">
    <div class="clear">
    <h2><img src="images/helpDeskIcon.png" align="absmiddle" width="40" style="border-radius:5px;"> Help Desk</h2>
    <p><img src="images/services/help-desk.jpg" class="left serviceimg">Need to configure, reconfigure 
    or update all your computers in your company but you lack of an IT department, need to install a network 
    printer for your employees, we have all the right solutions for that, our specialized tech support team can 
    resolve those issues for you, so now can attend what matters most - your customers.</p>
    </div>
    <div class="clear">
    <button class="viewmore backBtnService">Back</button>
    </div>
    </div>
    
</div>


</div> 

</div>
<?php include("aside.php"); ?>
</div>
</div>
</div>
<?php include("footer.php"); ?>

</div>
</body>
</html>