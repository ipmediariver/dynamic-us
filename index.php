<?php include("header.php"); ?>
<div class="fullblock container">
    <div class="fullblock ">
        <div class="blockwrap clear pheight serviceblock">
            <div class="left" style="margin:0px 20px 0px 77px;">
                <a href="collaboration.php" title="Collaboration Solutions">
                    <img src="images/icon1.png" width="100" class="iconsol">
                </a>
            </div>
            <div class="mwidth left">
                <a href="datacenter.php" title="Data Center Solutions">
                    <img src="images/icon2.png" width="100" class="iconsol">
                </a>
            </div>
            <div class="mwidth left">
                <a href="its.php" title="ITS">
                    <img src="images/icon3.png" width="100" class="iconsol">
                </a>
            </div>
            <div class="mwidth left">
                <a href="software.php" title="Software Solutions">
                    <img src="images/icon4.png" width="100" class="iconsol">
                </a>
            </div>
            <!-- <div class="mwidth left"><a href="electronic.php"><img src="images/icon5.png" width="100" class="iconsol"><div class="solicondesc"><p>Electronic<br> Security<br>Solutions</p></div></a></div> -->
            <div class="mwidth left">
                <a href="network.php" title="Network Security Solutions">
                    <img src="images/icon6.png" width="100" class="iconsol">
                </a>
            </div>
            <div class="mwidthleft left">
                <a href="wifi.php" title="Access and Wireless">
                    <img src="images/icon7.png" width="100" class="iconsol">
                </a>
            </div>
        </div>
    </div>
    <div class="fullblock">
        <div class="blockwrap clear pheight">
            <div class="left" style="width:960px;">

                <h1>Overview</h1>
                <p>For now, more than 18 years, <b>Dynamic Communications</b> has been an industry leader in Seamless IP Telephony Applications, Data Networks and Structured Cabling Solutions. Our customers range from Small Businesses to Large Multinational Companies, across a variety of Vertical Markets.</p>

                <p>We have the ability to tackle any size project in for our Multinational Customers who demand a Single Point of Contact for their solution design, purchase and deployment.</p>

                <p>Our Solution Portfolio has increased with time and we are proud that our products and Services Offering now includes Cloud Based Services and Software.</p>
                <a href="about.php" class="viewmore">View more</a>

                <!-- <div class="clear">
                    <div class="left" style="width:100%;">
                        <div style="padding-right:10px; height:190px; position:relative;">
                        </div>
                    </div>
                    <div class="left" style="width:50%;">
                        <div style="padding-left:10px; position:relative; cursor:pointer" class="mediaBtn corporateVideo">
                            <div class="playCorpVideo">
                                <iframe width="100%" height="295" class="corpVideo" src="https://drive.google.com/file/d/0B7bfVE6SUAMyNXJsRm5RbUpfRjg/preview?autoplay=1" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="clear mheight">
                    <a href="#video1" class="fancybox left relative mediaBtn" style="margin-right:50px;">
                        <img src="images/video3.jpg" class="borderRadius"><br>
                        <span class="videoDesc">Radvision by Avaya</span>
                        <img src="images/playIcon.png" class="absolute playIcon" style="opacity:.5; top:50%; margin-top:-50px; left:50%; margin-left:-30px;">
                    </a>
                    <a href="#video2" class="fancybox left relative mediaBtn" style="margin-right:50px;">
                        <img src="images/video4.jpg" class="borderRadius">
                        <span class="videoDesc">eVident</span>
                        <img src="images/playIcon.png" class="absolute playIcon" style="opacity:.5; top:50%; margin-top:-50px; left:50%; margin-left:-30px;">
                    </a>
                    <a href="#video3" class="fancybox left relative mediaBtn" style="margin-right:50px;">
                        <img src="images/video5.jpg" class="borderRadius"><br>
                        <span class="videoDesc">Plantronics</span>
                        <img src="images/playIcon.png" class="absolute playIcon" style="opacity:.5; top:50%; margin-top:-50px; left:50%; margin-left:-30px;">
                    </a>
                    <a href="#video4" class="fancybox left relative mediaBtn">
                        <img src="images/video7.jpg" class="borderRadius"><br>
                        <span class="videoDesc">Cisco</span>
                        <img src="images/playIcon.png" class="absolute playIcon" style="opacity:.5; top:50%; margin-top:-50px; left:50%; margin-left:-30px;">
                    </a>
                    <!-- <a href="#video4" class="fancybox left relative mediaBtn">
                        <img src="images/video6.jpg" class="borderRadius">
                        <span class="videoDesc">Cloud Computing</span>
                        <img src="images/playIcon.png" class="absolute playIcon" style="opacity:.5; top:50%; margin-top:-50px; left:50%; margin-left:-30px;">
                    </a> -->
                    <div id="video1" class="none">
                        <iframe width="650" height="370" src="https://www.youtube-nocookie.com/embed/4JUAugaVYXQ" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div id="video2" class="none">
                        <iframe width="650" height="370" src="https://www.youtube-nocookie.com/embed/6HSW59ny4z8" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div id="video3" class="none">
                        <iframe width="650" height="370" src="https://www.youtube-nocookie.com/embed/0iAhtDJncUk?rel=0" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div id="video4" class="none">
                        <iframe width="650" height="370" src="https://www.youtube-nocookie.com/embed/M578lU2TGeI?rel=0" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div id="video5" class="none">
                        <iframe width="650" height="370" src="https://drive.google.com/file/d/0B7bfVE6SUAMyNXJsRm5RbUpfRjg/preview" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="relative" style="background:#e2e6ec; border-radius:5px; border-bottom:1px solid #bcc4ce; padding:15px 20px 10px 10px; margin-top:20px;">
                    <h1 class="downloadPdfH1"><img src="images/arrowRightIcon.png" height="12"> Learn more about us, please request our <span class="redColor bold">Corporate Presentation</span></h1>
                    <a href="#javascript;" class="pdfDownloadBtn absolute">
                        Request
                    </a>
                </div>
                <div class="clear downloadPdfForm none mheight">
                    <form  id='frmDownload' class="pdfForm">
                        <div class="clear">
                            <div class="left" style="margin-right:10px;">
                                <p>Name</p>
                                <input type="text" name="txtName" id='txtName' class="val" required>
                            </div>
                            <div class="left" style="margin-right:10px;">
                                <p>Last Name</p>
                                <input type="text" name="txtApellido" id='txtApellido' class="val" required>
                            </div>
                            <div class="left" style="margin-right:10px;" >
                                <p>E-mail</p>
                                <input type="text" name="txtEmail" id='txtEmail' class="val" required>
                            </div>
                            <div class="left" style="margin-right:10px;">
                                <p>Phone</p>
                                <input type="text" name="txtTelefono" id='txtTelefono' class="val" required>
                            </div>
                            <div class="left">
                                <p>Company</p>
                                <input type="text" name="txtEmpresa" id='txtEmpresa' class="val" required>
                            </div>
                        </div>
                        <div class="clear relative" style="height:50px;">
                            <button class="readyDownloadPdf absolute">Request now!</button>
                        </div>
                    </form>
                </div>
                
            </div>
        </div>
    </div>
</div>
<?php include("footer.php"); ?>
</div>
</body>
</html>