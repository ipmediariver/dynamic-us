<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Dynamic Communications</title>
<meta name="title" content="Dynamic Communications at the Speed of Your Business. Welcome.">
<meta name="keywords" content="Dynamic Communications, Dynacom, Telecommunication, Telecommunications, Avaya, Cisco, IBM, Fiber Optic, Structured Cabling, Software, Cabling,">
<link rel="icon"  type="image/png" href="images/favicon.png">
<link href="css/general.css" rel="stylesheet" type="text/css">
<link href="css/reset.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="fonts/fonts.css" rel="stylesheet" type="text/css">
<link href="css/mquery.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="source/jquery.fancybox.css?v=2.0.6" media="screen" />  


<!--[if lt IE 9]>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->

<!--[if lt IE 8]>
	 <link href="ie.css" rel="stylesheet" type="text/css" />
<![endif]-->

</head>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="source/jquery.fancybox.js?v=2.0.6"></script>
<script type="text/javascript" src="js/script.js"></script>


<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<style type="text/css">
   .blockwrap{ width:960px !important; margin:0 auto;}
</style>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-11254633-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<!-- Start of Woopra Code -->
<script type="text/javascript">
function woopraReady(tracker) {
    tracker.setDomain('godynacom.com');
    tracker.setIdleTimeout(1800000);
    tracker.track();
    return false;
}
(function() {
    var wsc = document.createElement('script');
    wsc.src = document.location.protocol+'//static.woopra.com/js/woopra.js';
    wsc.type = 'text/javascript';
    wsc.async = true;
    var ssc = document.getElementsByTagName('script')[0];
    ssc.parentNode.insertBefore(wsc, ssc);
})();
</script>
<!-- End of Woopra Code -->

 <!-- Start of Woopra Code -->
<script type="text/javascript">
function woopraReady(tracker) {
    tracker.setDomain('godynacom.com');
    tracker.setIdleTimeout(1800000);

    // Make sure you add the visitor properties before the track() function.
    tracker.addVisitorProperty('name', '$account.name');
    tracker.addVisitorProperty('email', '$account.email');
    tracker.addVisitorProperty('company', '$account.company');

    tracker.track();
    return false;
}
(function() {
    var wsc = document.createElement('script');
    wsc.src = document.location.protocol+'//static.woopra.com/js/woopra.js';
    wsc.type = 'text/javascript';
    wsc.async = true;
    var ssc = document.getElementsByTagName('script')[0];
    ssc.parentNode.insertBefore(wsc, ssc);
})();
</script>
<!-- End of Woopra -->

<body>
<div class="pagewrap clear">
 

<?php include("login.php"); ?>
<?php include("sticket.php"); ?>
 
<div class="fullblock header pheight" style="padding-top: 20px;">

    <div class="blockwrap clear">
       <div class="left"><a href="index.php" style="position: relative; top: 5px;"><img src="images/logo_dynacom.svg" width="300"></a></div>
       <div class="right"><?php include("navigation.php"); ?></div>

    </div>

</div>
    
