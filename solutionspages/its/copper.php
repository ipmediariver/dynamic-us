<h1>Copper Cabling Solutions</h1>
<p>We bring the most reliable cabling solutions on the market as we implement them using the world’s best vendors, offering scalable, modular and certified cabling using the best practices to ensure your network´s infrastructure optimal performance all the time.
Our Copper Cabling solutions range from these 3 different categories:</p>

<ul class="list">
<li>Cat5e: The most popular cabling category, this cable can support the following speeds, 10MB/100MB/1000MB at three levels of shielding, UTP, FTP and SFTP.</li>
<li>Cat6:  This type of cabling solution is becoming very popular among customers with high traffic on their networks and it´s recommended for IP telephony implementation, this solution runs at natively at GigaSpeed (1000MB) at three levels of shielding – UTP, FTP and SFTP.</li>
<li>Cat6a: This solution is recommended for Ultra High Network traffic or Data Center Environments this cable speed is Ten Gigaspeed (10,000MB), these cables are shielded in the following standards SFTP, FFTP and FUTP.</li>
</ul>

<p>We install:</p>

<ul class="logoslist">
	<li><img src="images/LOGOS/Commscope_Logo.jpg" height="40" style="margin-top:0px;"></li>
    <li><img src="images/LOGOS/panduit-logo.jpg" height="30" style="margin-top:5px;"></li>
    <li><img src="images/LOGOS/AMP_NETCONNECT_logo.jpg" height="40" style="margin-top:0px;"></li>
</ul> 