<h1>Web Development and Design</h1>

<h2>Design</h2>
<p>The identity of a company is its image, the main idea that stays with the consumer, acting as one of the main sales factors, this is why we partner with IP media river to deliver quality design to your company always having in mind the latest consumer design trends to ensure that your image stays fresh several years from now.</p>
<h2>Web Development</h2>
<p>Need a tailored productivity solution, we can help you accomplish your goals with our web and software developing team that understands your needs and knows how to translate those requirements in a business enhancing application for your company stored in the cloud.</p>
<p>This solution is developed with:</p>

<ul class="logoslist">
	<li><img src="images/LOGOS/ipmediariver.png" height="40" style="margin-top:0px;"></li>
</ul> 

