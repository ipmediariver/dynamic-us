<h1>Licensing</h1>

<p>We have partnered with the industry´s leader in software and operational systems, yes Microsoft, we deliver high quality solutions that will be constantly updated to ensure optimal productivity all the time, without risking compatibility on the process, our licensing solutions are:</p>

<ul class="list">
<li>Operating systems for PC and servers</li>
<li>Productivity software such as office </li>
</ul>

<p>Solutions provided with the help of:</p>
<ul class="logoslist">
	<li><img src="images/LOGOS/microsoft.png" height="40" style="margin-top:0px;"></li>
</ul> 

