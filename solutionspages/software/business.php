<h1>Business Apps</h1>

<p>Need a CRM or an ERP application for your company? We’ve got you covered, are you in need of a tailored solution that you can´t find nowhere else? Don´t look any further, we have a team of specialized programmers that can help you leverage an integrated solution made just for you.</p>
<p>This solution is developed with:</p>
<ul class="logoslist">
	<li><img src="images/LOGOS/ipmediariver.png" height="40" style="margin-top:0px;"></li>
</ul> 


