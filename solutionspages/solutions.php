<h1>Solutions</h1>

<p>We always integrate the best IT solutions on the market that bring value to your business, throughout our more than 13 years on the business we have managed to incorporate the right solution portfolio that will be able to suit your IT needs.<br><br>
We have narrowed our solutions down to the following categories:</p>

<div class="clear pheightbottom" style="border-bottom:1px solid #ccc;">
<div class="mwidthright left"><a href="collaboration.php"><img src="images/icon1.png" width="80" class="iconsol"><div class="solicondesc"><p>Collaboration<br> Solutions</p></div></a></div>

<div class="mwidthright left"><a href="datacenter.php"><img src="images/icon2.png" width="80" class="iconsol"><div class="solicondesc"><p>Data Center<br> Solutions</p></div></a></div>

<div class="mwidthright left"><a href="its.php"><img src="images/icon3.png" width="80" class="iconsol"><div class="solicondesc"><p>ITS</p></div></a></div>

<div class="mwidthright left"><a href="software.php"><img src="images/icon4.png" width="80" class="iconsol"><div class="solicondesc"><p>Software<br>Solutions</p></div></a></div>

<!-- <div class="mwidthright left"><a href="electronic.php"><img src="images/icon5.png" width="80" class="iconsol"><div class="solicondesc"><p>Electronic<br> Security<br>Solutions</p></div></a></div> -->

<div class="mwidthright left"><a href="network.php"><img src="images/icon6.png" width="80" class="iconsol"><div class="solicondesc"><p>Network<br> Security<br> Solutions</p></div></a></div>

<div class="mwidthright left"><a href="wifi.php"><img src="images/icon7.png" width="80" class="iconsol"><div class="solicondesc"><p>Access<br> and Wireless</p></div></a></div>
</div>