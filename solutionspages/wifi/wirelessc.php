<h1>Wireless Campus Infrastructure (Mesh)</h1>

<p>If you require delivering wireless access to a big campus, bigger solutions are always recommended, this is when Wireless Mesh comes in to play, and this is wireless interconnected network with several points within a perimeter allowing a much more wider access to wireless connectivity, this solution is recommended for schools, malls, theme parks, government buildings, Industrial complexes, shipyards and manufacture companies that have to provide wireless access to a really big area.</p>

<p>This solution is implemented with:</p>

<ul class="logoslist">
	<li><img src="images/LOGOS/cisco.png" height="50" style="margin-top:0px;"></li>
</ul> 

