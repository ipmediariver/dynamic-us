<h1>WiMAX</h1>

<p>It´s your business perimeter so large that requires an unparallel solution? If this is the case we introduce WiMAX (802.16b standard) a solution that runs on a different wireless frequency (3.5GHz and 5.8GHz) designed to reach longer distances than regular WiFi, this solution is required for MAN (Metropolitan Area Network) or WAN (Wide Area Network) infrastructures, it can also be implemented for rural or desert areas were building a cabled infrastructure can be very expensive. </p>
<p>This solution can be achieved with:</p>

<ul class="logoslist">
	<li><img src="images/LOGOS/cisco.png" height="50" style="margin-top:0px;"></li>
</ul> 

