<h1>Firewall</h1>

<p>These systems are mandatory in today´s site and data center environments in which threats are an everyday problem, these filters or firewalls as they are commonly known are responsible of managing inbound as outbound network traffic to secure any vulnerability that your network may have, some firewalls can even adapt to the threat response making it harder to the attacker, stopping the attack from even starting.</p>

<p>Our firewall solutions are implemented with:</p>
<p>WatchGuard, IBM, JUNIPER</p>

<ul class="logoslist">
	<li><img src="images/LOGOS/watchguard.png" height="40" style="margin-top:0px;"></li>
    <!-- <li><img src="images/LOGOS/ibm.png" height="40" style="margin-top:0px;"></li> -->
    <li><img src="images/LOGOS/juniper.png" height="40" style="margin-top:0px;"></li>
</ul> 