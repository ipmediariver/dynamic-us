<h1>Unified Collaboration</h1>
<p>
The open architecture of today´s communication solutions allow a broader access to different kinds of tools that allow your employees to enhance their productivity even further, with unified communications you can be in touch with anyone in your company no matter which communication method they are using, whether by voice, message, SMS, email, chat they become available to your any time anywhere.
</p>
<p>These solutions can be provided by the following brands:</p>
<ul class="logoslist">
	<li><img src="images/LOGOS/avaya.png" style="margin-top:20px;"></li>
	<li><img src="images/LOGOS/cisco.png"></li>
</ul>    

