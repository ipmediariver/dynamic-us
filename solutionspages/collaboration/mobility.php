<h1>Mobility Solutions</h1>
<p>
Ever thought of changing the business environment for your mobile employees, giving them the freedom to communicate from wherever they are, with any device, even with one brought by their own (BYOD), on a system seamlessly integrated to your core communications solution.<br />
Mobility solutions can help you accomplish that, without the need of even changing your current internal communication solution.
<p>
<p>This solution is provided by:</p>
<ul class="logoslist">
	<li><img src="images/LOGOS/avaya.png" style="margin-top:20px;"></li>
	<li><img src="images/LOGOS/cisco.png"></li>
</ul>    


