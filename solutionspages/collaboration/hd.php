<h1>HD Video Presence</h1>
<p>
Get close and personal at your business meetings around the globe without leaving your office, HD Video Presence brings you a new level of interaction with your colleagues, business partners and customers without the necessity of travel, reducing overall cost, saving you travel time, and improving your business productivity.
<p>
<p>Key Dynamic Communications Facts about this solution:</p>
<ul class="list">
	<li>We have the latest and most advanced HD Video Collaboration Solutions in the market today. </li>
	<li>Enjoy Full HD Video Collaboration with lower Bandwidth required </li>
	<li>Easy to use and deploy. </li>
</ul>


<p>This solution is deployed using:</p>
<ul class="logoslist">
	<li><img src="images/radvisionLogo.jpg" height="40" style="margin-top:0px;"></li>
</ul>    

