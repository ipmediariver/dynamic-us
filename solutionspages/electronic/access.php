<h1>Access and Perimeter Control</h1>
<p>Yes you wish to grand access to your employees but what it´s the level of clearance they have within your business perimeter? Are they allowed to wonder around between departments? Do you have critical systems and information located in your site? You can help resolve all of these questions by implementing an advanced access and perimeter control system that will allow only authorized access to the correct area any time.</p>
<p>This solution is configured using:</p>


<ul class="logoslist">
	<li><img src="images/LOGOS/honeywell-logo.jpg" height="30" style="margin-top:5px;"></li>
    <li><img src="images/LOGOS/HID_logo.jpg" height="40" style="margin-top:0px;"></li>
    <li><img src="images/LOGOS/tycointegratedsecurity-logo_10737409.jpg"  height="40" style="margin-top:0px;"></li>
</ul> 

