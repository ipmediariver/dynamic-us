<h1>Intelligent Business Protection Systems</h1>

<p>When regular security systems are not enough to secure your business holdings, stakeholders can be at risk of losing valuable patrimonial or infrastructural belongings among other things; this is why Dynamic Communications can offer intelligent Business Protection systems to ensure your company´s safety all the time.</p>

<p>Our intelligent Business Protection systems include:</p>

<ul class="list">
<li>Advanced Video Analytics</li>
<li>RFID Tags and Survey</li>
<li>Netbotz</li>
<li>Fire and alarm system automatization.</li>
</ul>

<p>This solution is implemented using:</p>

<ul class="logoslist">
	<li><img src="images/LOGOS/honeywell-logo.jpg" height="30" style="margin-top:5px;"></li>
    <li><img src="images/LOGOS/HID_logo.jpg" height="40" style="margin-top:0px;"></li>
    <li><img src="images/LOGOS/tycointegratedsecurity-logo_10737409.jpg" height="40" style="margin-top:0px;"></li>
    <li><img src="images/LOGOS/apc.png" height="40" style="margin-top:0px;"></li>
</ul> 