<div class="right aside">


<img src="images/img1.png">
<p><span class="bold">SCOPIA XT Telepresence</span><br>Immersive Telepresence Customized to Your Unique Requirements</p>
<a href="#asideCont" class="fancybox viewmore">view more</a>
</div>

<div class="none" id="asideCont" style="width:650px; height:550px;">


<div style="overflow:auto; overflow-x:hidden; width:630px; padding-right:20px; height:550px;">
<img src="images/img1.png" class="left" style="width:220px; margin:0px 10px 0px 0px; border:1px solid #ccc;">	
<h4>Immersive Telepresence Customized to Your Unique Requirements</h4> 
<p>The <b>SCOPIA XT Telepresence Platform</b> delivers an immersive telepresence experience customizable to the unique 
requirements of individual rooms and customer needs.  Installed and configured by RADVISION’s worldwide network 
of channel partners, the <b>SCOPIA XT Telepresence Platform</b> provides a cost-effective and highly flexible approach.
</p>
<p>With a <b>RADVISION</b> partner, customers can define the installation and furnishings that best fit within their 
conference room and budgets to deliver a distinct customized telepresence experience.</p>

<h4>SCOPIA Telepresence Highlights</h4>

<p class="bold">Immersive Life-like Experience</p>
<p>Replicate the feeling of meeting in person.  The SCOPIA XT Telepresence Platform incorporates state of-the-art technology 
with support for full HD 1080p video, 48 kHz full band audio and 1080p 30fps PC data sharing for an immersive life-like experience.</p>

<p class="bold">Customizable and Flexible</p>
<p>Delivers an immersive telepresence experience tailored to the unique requirements of individual rooms and customer needs.  
Installed and configured by our worldwide network of channel partners, the Platform provides a cost-effective and flexible 
telepresence deployment approach with optimal quality in all conditions ensured through powerful calibration and setup tools.</p>

<p class="bold">Market Leading Interoperability</p>
<p>The SCOPIA XT Telepresence Platform combined with the SCOPIA Elite MCU delivers unmatched interoperability with telepresence 
systems from Cisco®/Tandberg®, Logitech®/LifeSize® and Polycom® as well as with any standards-based video conferencing system 
for full video, audio and H.239 data collaboration.</p>

<p class="bold">Intuitive Apple iPad Control</p>
<p>The learning curve for using the SCOPIA XT Telepresence Platform is virtually eliminated with the SCOPIA Control Multi-Touch™ 
application for the Apple iPad®.  First time users can initiate calls, control their SCOPIA XT Telepresence systems and moderate 
meetings without any training or introduction.</p>

<p class="bold">Breakthrough Price Point</p>
<p>The Telepresence Platform stands out amongst the competition delivering immersive collaboration capabilities for a fraction of 
what competitors charge.</p>

</div>


</div>