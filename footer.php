<div class="fullblock footerblock">
        <div class="blockwrap clear pheightbottom">
                <div class="boxfooter left mwidthright aboutusfooter">
                   <h3>About Us:<hr></h3>
                   <ul class="footerlink">
                      <li><a href="about.php">Company Overview</a></li>
                      <li><a href="history.php">History</a></li>
                      <!-- <li><a href="offices.php">Dynacom Offices</a></li> -->
                      <li><a href="partners.php">Partners</a></li>
                      <li><a href="certifications.php">Certifications</a></li>
                   </ul>
                </div>
                <div class="boxfooter left mwidthright">
                   <h3>Solutions:<hr></h3>
                   <ul>
                      <li><a href="collaboration.php">Collaboration</a></li>
                      <li><a href="datacenter.php">Data Center</a></li>
                      <li><a href="its.php">ITS</a></li>
                      <li><a href="software.php">Software Solutions</a></li>
                      <!-- <li><a href="electronic.php">Electronic Security Solutions</a></li> -->
                      <li><a href="network.php">Network Security Solutions</a></li>
                      <li><a href="wifi.php">Access and Wireless</a></li>
                   </ul>
                </div>
                <div class="boxfooter left mwidthright">
                   <h3>Services:<hr></h3>
                   <ul>
                      <li><a href="services.php">IT Consulting</a></li>
                      <li><a href="services.php">Maintenance Contracts</a></li>
                      <li><a href="services.php">Network assessment</a></li>
                      <li><a href="services.php">IT Equipment install/config.</a></li>
                      <li><a href="services.php">Help Desk</a></li>
                   </ul>
                </div>
                <div class="boxfooter left mwidthright">
                   <h3>Partners:<hr></h3>
                   <ul>
                      <li><a href="http://www.avaya.com/mx/" target="_blank">Avaya</a></li>
                      <li><a href="http://www.cisco.com/" target="_blank">Cisco</a></li>
                      <li><a href="http://www.commscope.com/company/eng/index.html" target="_blank">Commscope </a></li>
                      <!-- <li><a href="http://www.ibm.com" target="_blank">IBM </a></li> -->
                      <li><a href="http://www.panduit.com/index.htm" target="_blank">Panduit </a></li>
                      <li><a href="http://www.schneider-electric.com/site/home/index.cfm/ww/?selectCountry=true" target="_blank">Schneider</a></li>
                      <li><a href="http://www.belden.com/" target="_blank">Belden</a></li>
                      <!-- <li><a href="http://www.tyco.com/wps/wcm/connect/tyco+home/Home/" target="_blank">Tyco</a></li> -->
                   </ul>
                </div>
                <div class="boxfooter left" style="position:relative; min-height:270px;">
                   <h3>Contact:<hr></h3>
                   <ul>
                      <li><a href="contact.php">Contact</a></li>
                      <li><a href="jobs.php">Jobs</a></li>
                    </ul>
                    
                    <div class="clear" style="position:absolute; bottom:0px;">
                      <!--
                      SiteSeal Html Builder Code:
                      Shows the logo at URL https://seal.networksolutions.com/images/basicrecblue.gif
                      Logo type is  ("NETSB")
                      //-->
                      <!-- <script language="JavaScript" type="text/javascript"> SiteSeal("https://seal.networksolutions.com/images/basicrecblue.gif", "NETSB", "none");</script> -->
                    </div>
                    
                </div>
        </div>
</div>
<div class="fullblock credits">
    <div class="blockwrap">
       <p>Dynamic Communications 2017 Rights Reserved. Development by IP Media River ©</p>
    </div>
</div>