<?php include("headertwo.php"); ?>

<div class="fullblock sliderblock">
<div class="blockwrap">
<img src="images/Blog-and-social-media.jpg" align="absmiddle" class="firstimage">
</div>



</div>
<div class="blockwrap sliderbarblue">

<div class="blockwrap clear pwidthleft">

</div>

</div>


<div class="fullblock container">


<div class="fullblock">
<div class="blockwrap clear pheight">
<div class="left article">
<div id="contents">
<img src="images/PTL-Banner1.jpg" width="100%">  
<h2>Dynamic Communications and Plantronics Deliver Flexible Communications Tools for the office</h2>
<p>
<var>New Mobility Products for Business Professionals Work Across Mobile Phones, Laptops and Tablets</var><br>
October 25, 2012 – We at Dynamic Communications™ are commited to our customers and this is why we always 
bring the most innovative solutions on the market, and becuase of that we are proud to introduce Plantronics® 
to our Collaboration Solutions, starting from now you will be able to order any Plantronics® brand Headset wherever 
you need a call-center solution peace, business  or personal use, we have the headset that its right for you.
</p>

<h2>Changing the work environmet to a collaboration space</h2>

<p>
“For many professionals, conducting business from a cubicle or closed office environment has become a thing of 
the past. Work has become much more about what someone does and a lot less about where they do it,” said Rob Arnold, 
Senior Analyst, Frost & Sullivan. “ With that said, it’s important for companies to equip their mobile professionals 
with the latest solutions that not only give them the flexibility to communicate well regardless of where they are, 
but that also make the user experience as intuitive as possible, and that’s something Plantronics has down to a science.”
</p>

<img src="images/PTL-Banner2.jpg" width="100%">
<h2 style="margin:0px;"><var>“Cubicle or closed office environment has become a thing of the past”</var></h2>
<samp>-Rob Arnold</samp>

<p>Plantronics suite of business mobility products include:</p>

<p class="bold">Voyager Legend UC – The Next Generation of an Iconic Communications Device</p>

<p>Plantronics Voyager Legend UC is the award-winning Voyager family’s latest solution which connects to a laptop, mobile 
phone or tablet. This headset understands how and when you want to talk, with industry-first Smart Sensor™ technology 
that reacts when you put the headset on, letting you quickly take a call without a click. For more information, 
<a href="http://launchkit.plantronics.com/products/voyager-legend-uc/">Click here</a></p>

<p class="bold">Blackwire® Series – The Crossover Headset Where Corded Reliability Meets Wireless Flexibility</p>

<p>This versatile headset goes beyond providing outstanding PC audio quality by also connecting to a Bluetooth enabled phone 
or tablet. The Blackwire 700 series is the first corded headset to feature Plantronics’ Smart Sensor technology, which tells 
the headset when it is being worn so it can pause music and route calls intuitively. For more information, 
<a href="http://www.plantronics.com/product/blackwire-700">Click here</a></p>

<p class="bold">Calisto® 620 – The No Cord, No Fuss Speakerphone</p>

<p>The first wireless speakerphone designed for use with your laptop or smartphone, Calisto 620 is portable, sounds great and 
sets up easily. For more information, <a href="http://www.plantronics.com/product/calisto-600">Click here</a></p>

<p>
<var>“Work is something you do, not a place you go”, said Amy Huson, senior director of enterprise marketing at Plantronics. 
“In a typical week, I find myself working from home, the car, and office but I need to sound professional all the time.  
These new enterprise mobility products help people communicate effectively wherever the day takes them.” </var>
</p>

<img src="images/PLT-Banner-3.jpg" width="100%">
<h2 style="margin:0px;"><var>“Work is something you do, not a place you go”</var></h2>
<samp>-Amy Huson</samp>

<p>Voyager Legend UC will be available in January 2013. Blackwire 710 (monaural) and Blackwire 720 (stereo) are available now. 
Calisto 620 is available now. These enterprise mobility products are available through us at Dynamic Communications™.</p>

<p>If you would like to receibe a quote please contact us:</p>

<p><span class="bold">USA:</span> (619) 616 2850<br>
<span class="bold">MX:</span> +52 (664) 621 1100<br>
<span class="bold">Email:</span> <var>info@godynacom.com</var></p>



</div> 

</div>
<?php include("aside.php"); ?>
</div>
</div>
</div>
<?php include("footer.php"); ?>

</div>
</body>
</html>