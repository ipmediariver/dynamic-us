<h1>Support</h1>
<div class="clear">
    <div class="boxcontactform mheightbottom">
        <h2><img src="images/contactbtns/SupportT.png" height="25" class="mwidthright" align="absmiddle">Tech Support</h2>
        
        
        <div class="clear techsupportbox" style="padding:10px;">
            
            <div class="clear" style="border-bottom:1px solid #ccc; padding:10px 10px;">
                <div class="left" style="width:200px;">
                    <p><span class="bluetext">Support Email</span></p>
                </div>
                <div class="left" style="width:200px;">
                    <p>Monday to Friday<br>
                    8:00 to 18:30 hrs.</p>
                </div>
                <div class="left" style="width:200px;">
                    <p><b>support@godynacom.com</b></p>
                </div>
            </div>
            
            <div class="clear" style="border-bottom:1px solid #ccc; padding:10px 10px;">
                <div class="left" style="width:200px;">
                    <p><span class="bluetext">Service and <br>
                        Support
                    Center</span></p>
                </div>
                <div class="left" style="width:200px;">
                    <p>Monday to Friday<br>
                    8:00 to 18:30 hrs.</p>
                </div>
                <div class="left" style="width:200px;">
                    <p><b>support@godynacom.com</b></p>
                </div>
            </div>
            
            <div class="clear" style="border-bottom:1px solid #ccc; padding:10px 10px;">
                <div class="left" style="width:200px;">
                    <p><span class="bluetext">Web<br>
                        www.godynacom.com<br>
                    /service </span></p>
                </div>
                <div class="left" style="width:200px;">
                    <p>24 Hours</p>
                </div>
                <div class="left" style="width:200px;">
                    <p>Login Service Ticket<br>
                    <b>User/password</b></p>
                </div>
            </div>
            
            <div class="clear" style="padding:10px 10px;">
                <div class="left" style="width:200px;">
                    <p><span class="bluetext">Emergency<br>
                    Responce TEAM</span></p>
                </div>
                <div class="left" style="width:200px;">
                    <p>Holidays and Weekends </p>
                </div>
                <div class="left" style="width:200px;">
                    <p><span class="bluetext">(619)</span> <b>864-7685</b></p>
                </div>
            </div>
            
            
            
        </div>
        
        
        
    </div>
    <div class="boxcontactform">
        <h2><img src="images/contactbtns/CustomerCare.png" height="25" class="mwidthright" align="absmiddle">Sales & Customer Care</h2>
        <p><b>US (619) 616 2850</b></p>
        </div>
    </div>