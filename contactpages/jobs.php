

<script type="text/javascript">

$(document).ready(function(e) {
   $(".jobsbtn li a").click(function(){
	  var theJob = $(this).attr("alt");  
	  $(".btnjob").removeClass("act");
	  $(this).addClass("act"); 
	  $(".jobs" + theJob).html($(this.hash).html())  
	  $(".jobs" + theJob).show();
	  $(".closejobs" + theJob).show(); 
   }) 
   $(".btnclose").click(function(){
	  var closeJob = $(this).attr("alt");  
	  $(".jobs" + closeJob).hide();  
	  $(this).hide(); 
	  $(".jobsbtn li a").removeClass("act");
   })
});


</script>


<h1>Jobs</h1>

<div class="clear">
  
   <!--1 job--> 
   <div class="boxcontactform" style="margin-bottom:15px;">
   <h2>IT Account Manager</h2>
   <div class="clear" style="padding:10px 0px;">
   <p>
   	
Consultant on State-of-the-art IT and collaboration solutions follow up and support multiple customers at the same time and provide state of the art solutions on all customers’ needs. 

   </p>
   
   
   <div class="clear">
   
   <!--Requirements-->
   <div class="clear pwidth">
   <div class="clear">
   <ul class="jobsbtn">
   <li><a href="#dutie1" class="btnjob" alt="no1">Duties</a></li>
   <li><a href="#req1" class="btnjob" alt="no1">Requirements</a></li>
   </ul>
   </div>
   <div class="clear jobsno1 mheight" style="display:none;">
   
   </div>
   <a href="#javascript;" class="blacktext btnclose closejobsno1" style="display:none;" alt="no1">Hide specifications</a>
   <div id="dutie1" style="display:none">
      <ul class="list">
        <li>Responsibility of handling company customers</li>
        <li>Generate demand for new and existing customers</li>
        <li>Generate rapport for assigned accounts</li>
        <li>Attend meetings about company strategies</li>
        <li>Follow marketing alignments</li>
        <li>Give customer support to accounts under his/her control</li>
        <li>Meet company's deadlines on time</li>
     </ul>
   </div>
   <div id="req1" style="display:none">
   <ul class="list">
        <li>Bachelor degree in computer science, networking, IT engineering or similar is a must</li>
        <li>Knowledge in the latest technology trends</li>
        <li>Likeness for technology, networking, gadgets among other things</li>
        <li>Business oriented</li>
        <li>Dynamic</li>
        <li>Highly responsible</li>
        <li>Proactive</li>
        <li>Team player</li>
        <li>Fully bilingual in English and Spanish</li>
        <li>Works great under pressure</li>
        <li>Enthusiasm for sales</li>
        <li>Valid Driver's license & US Visa </li>
        <li>Owns an automobile and has no issues in using it for company tasks</li>
   </ul>
   </div>
   </div>
   <!--Requirements-->
   
   
   <p><span class="bluetext">Apply for this vacancy</span></p>
   </div>
   <div class="clear" style="margin-top:-15px;">
   <form method="post" style="padding:10px 20px;" class="jobsform">
      <div class="clear left">
      <input type="text" class="left mwidthright inputjobs" name="name" placeholder="Name">
      <input type="text" class="left inputjobs" name="email" placeholder="E-mail">
      </div>
      <div class="clear left">
      <input class="uploadcv" type="file" value="Upload CV" name="upload"><button type="submit" class="sendmessage">Upload File</button><br>
      <span class="tooltiptext">Upload your CV - (Only <b>PDF</b> Files)</span>
      </div>
   </form>
   </div>
   </div>
   </div>
   <!--1 job--> 
   
   <!--2 job--> 
   <div class="boxcontactform" style="margin-bottom:15px;">
   <h2>IT Service Engineer</h2>
   <div class="clear" style="padding:10px 0px;">
   <p>
   As service engineer you will provide technical service and support to new and existing customer networks - telecom and datacom or internal test. You will plan, design, troubleshoot and resolve problems in order to stabilize and optimize customer networks. You will travel nationally and internationally when necessary.
   </p>
   
   
   <div class="clear">
   
   <!--Requirements-->
   <div class="clear pwidth">
   <div class="clear">
   <ul class="jobsbtn">
   <li><a href="#dutie2" class="btnjob" alt="no2">Duties</a></li>
   <li><a href="#req2" class="btnjob" alt="no2">Requirements</a></li>
   </ul>
   </div>
   <div class="clear jobsno2 mheight" style="display:none;">
   
   </div>
   <a href="#javascript;" class="blacktext btnclose closejobsno2" style="display:none;" alt="no2">Hide specifications</a>
   <div id="dutie2" style="display:none">
      <ul class="list">
        <li>Responsible for providing technical and customer support by phone or at the customer´s site.</li>
        <li>Follow our company´s technical assistance and repair alignments.</li>
        <li>Generate rapport about customers serviced.</li>
        <li>Attend meetings to follow-up customer status.</li>
        <li>Meet company’s deadlines in time.</li>

     </ul>
   </div>
   <div id="req2" style="display:none">
   <ul class="list">

        <li>Bachelor degree in Computer Science, Networking, IT Engineering, Telecommunication Science Engineering or similar is a must.</li>
        <li>Knowledge in the latest technology trends.</li>
        <li>Cisco/Avaya certifications are desired.</li>
        <li>Knowledge/Experience with IP telephony.</li>
        <li>Knowledge in networking concepts.</li>
        <li>Familiarity with Data Center type networks.</li>
        <li>Likeness for technology, networks, gadgets among other things.</li>
        <li>Service oriented.</li>
        <li>Dynamic.</li>
        <li>Highly responsible.</li>
        <li>Proactive.</li>
        <li>Team player.</li>
        <li>Fully bilingual in English and Spanish (third language is a plus).</li>
        <li>Works great under pressure.</li>
        <li>Time availability.</li>



   </ul>
   </div>
   </div>
   <!--Requirements-->
   
   
   <p><span class="bluetext">Apply for this vacancy</span></p>
   </div>
   <div class="clear" style="margin-top:-15px;">
   <form method="post" style="padding:10px 20px;" class="jobsform">
      <div class="clear left">
      <input type="text" class="left mwidthright inputjobs" name="name" placeholder="Name">
      <input type="text" class="left inputjobs" name="email" placeholder="E-mail">
      </div>
      <div class="clear left">
      <input class="uploadcv" type="file" value="Upload CV" name="upload"><button type="submit" class="sendmessage">Upload File</button><br>
      <span class="tooltiptext">Upload your CV - (Only <b>PDF</b> Files)</span>
      </div>
   </form>
   </div>
   </div>
   </div>
   <!--2 job-->
   
</div>