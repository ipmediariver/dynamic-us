<?php include("headertwo.php"); ?>
<div class="fullblock container" style="margin-top:118px;">
	<div class="fullblock">
		<div class="blockwrap clear pheight">
			<div class="left article">
				<div id="contents">
				   <h1>Intranet</h1>
				   <script type="text/javascript">
                       $(document).ready(function(){
                       	 $('.listIntranet li a.item').click(function(){
                       	 	 var thisItem = $(this).parent().find('.listFiles');
                       	 	 if(thisItem.is(':visible')){
                       	 	 	return false
                       	 	 }else{
                       	 	 	$('.listFiles').slideUp();
                                $(this).parent().find('.listFiles').slideDown();
                       	 	 }
                       	 });
                       	 $('.listFiles').first().slideDown();
                       }) 
				   </script>
				   <ul class="listIntranet">
				   	<li>
				   	    <a href="#" class="item">Wallpapers</a>
				   	    <div class="listFiles">
				   	    	<ul class="thumbnails">
				   	    		<li>
				   	    		    <div class="thumInd">
				   	    		       <div class="thumbImg" style="background-image:url(images/intranet/wallpapers/18AnniversaryWall.jpg); background-repeat:no-repeat; background-size:cover; background-position:center;"></div>
				   	    		       <p>18 Anniversary Wall</p>
				   	    		       <a href="images/intranet/wallpapers/18AnniversaryWall.jpg" target="_blank" class="downloadItem"><img src="images/downloadIcon.png" align="absmiddle" style="height:12px">  Download</a>
				   	    		    </div>    
                                </li>
				   	    		<li>
				   	    		    <div class="thumInd">
				   	    		       <div class="thumbImg" style="background-image:url(images/intranet/wallpapers/18AnniversaryWall1.jpg); background-repeat:no-repeat; background-size:cover; background-position:center;"></div>
				   	    		       <p>18 Anniversary Wall 1</p>
				   	    		       <a href="images/intranet/wallpapers/18AnniversaryWall1.jpg" target="_blank" class="downloadItem"><img src="images/downloadIcon.png" align="absmiddle" style="height:12px">  Download</a>
				   	    		    </div>    
                                </li>
                                <li>
				   	    		    <div class="thumInd">
				   	    		       <div class="thumbImg" style="background-image:url(images/intranet/wallpapers/18AnniversaryWall2.jpg); background-repeat:no-repeat; background-size:cover; background-position:center;"></div>
				   	    		       <p>18 Anniversary Wall 2</p>
				   	    		       <a href="images/intranet/wallpapers/18AnniversaryWall2.jpg" target="_blank" class="downloadItem"><img src="images/downloadIcon.png" align="absmiddle" style="height:12px">  Download</a>
				   	    		    </div>    
                                </li>
                                <li>
				   	    		    <div class="thumInd">
				   	    		       <div class="thumbImg" style="background-image:url(images/intranet/wallpapers/18AnniversaryWall3.jpg); background-repeat:no-repeat; background-size:cover; background-position:center;"></div>
				   	    		       <p>18 Anniversary Wall 3</p>
				   	    		       <a href="images/intranet/wallpapers/18AnniversaryWall3.jpg" target="_blank" class="downloadItem"><img src="images/downloadIcon.png" align="absmiddle" style="height:12px">  Download</a>
				   	    		    </div>    
                                </li>
                                <li>
				   	    		    <div class="thumInd">
				   	    		       <div class="thumbImg" style="background-image:url(images/intranet/wallpapers/18AnniversaryWallEnglish.jpg); background-repeat:no-repeat; background-size:cover; background-position:center;"></div>
				   	    		       <p>18 Anniversary Wall English</p>
				   	    		       <a href="images/intranet/wallpapers/18AnniversaryWallEnglish.jpg" target="_blank" class="downloadItem"><img src="images/downloadIcon.png" align="absmiddle" style="height:12px">  Download</a>
				   	    		    </div>    
                                </li>
                                <li>
				   	    		    <div class="thumInd">
				   	    		       <div class="thumbImg" style="background-image:url(images/intranet/wallpapers/Ipad18thAnniversaryWall.jpg); background-repeat:no-repeat; background-size:cover; background-position:center;"></div>
				   	    		       <p>Ipad 18th Anniversary Wall</p>
				   	    		       <a href="images/intranet/wallpapers/Ipad18thAnniversaryWall.jpg" target="_blank" class="downloadItem"><img src="images/downloadIcon.png" align="absmiddle" style="height:12px">  Download</a>
				   	    		    </div>    
                                </li>
				   	    	</ul>
				   	    </div>
				   	</li>
				   </ul>
				</div>
			</div>
			<?php include("aside.php"); ?>
		</div>
	</div>
</div>
<?php include("footer.php"); ?>

</div>
</body>
</html>