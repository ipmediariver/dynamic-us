<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Dynamic Communications</title>
        <meta name="title" content="Dynamic Communications at the Speed of Your Business. Welcome.">
        <meta name="keywords" content="Dynamic Communications, Dynacom, Telecommunication, Telecommunications, Avaya, Cisco, IBM, Fiber Optic, Structured Cabling, Software, Cabling,">
        <link rel="icon"  type="image/png" href="images/favicon.png">
        <link href="css/general.css" rel="stylesheet" type="text/css">
        <link href="css/reset.css" rel="stylesheet" type="text/css">
        <link href="css/style.css" rel="stylesheet" type="text/css">
        <link href="fonts/fonts.css" rel="stylesheet" type="text/css">
        <link href="css/mquery.css" rel="stylesheet" type="text/css">
        <!-- <link rel="stylesheet" id="tw-bootstrap-css" href="bootstrap.min.css?ver=2.0.3" type="text/css" media="all"> -->
        <link rel="stylesheet" type="text/css" href="source/jquery.fancybox.css?v=2.0.6" media="screen" />
        <!--[if lt IE 9]>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <![endif]-->
        <!--[if lt IE 8]>
        <link href="ie.css" rel="stylesheet" type="text/css" />
        <![endif]-->
    </head>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
    <script type="text/javascript" src="source/jquery.fancybox.js?v=2.0.6"></script>
    <script language="JavaScript" src="https://seal.networksolutions.com/siteseal/javascript/siteseal.js" type="text/javascript"></script>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <style type="text/css">
    .blockwrap{ width:960px !important; margin:0 auto;}
    </style>
    <script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-11254633-3']);
    _gaq.push(['_trackPageview']);
    (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
    </script>
    <!-- Start of Woopra Code -->
    <script type="text/javascript">
    function woopraReady(tracker) {
    tracker.setDomain('godynacom.com');
    tracker.setIdleTimeout(1800000);
    tracker.track();
    return false;
    }
    (function() {
    var wsc = document.createElement('script');
    wsc.src = document.location.protocol+'//static.woopra.com/js/woopra.js';
    wsc.type = 'text/javascript';
    wsc.async = true;
    var ssc = document.getElementsByTagName('script')[0];
    ssc.parentNode.insertBefore(wsc, ssc);
    })();
    </script>
    <!-- End of Woopra Code -->
    <!-- Start of Woopra Code -->
    <script type="text/javascript">
    function woopraReady(tracker) {
    tracker.setDomain('godynacom.com');
    tracker.setIdleTimeout(1800000);
    // Make sure you add the visitor properties before the track() function.
    tracker.addVisitorProperty('name', '$account.name');
    tracker.addVisitorProperty('email', '$account.email');
    tracker.addVisitorProperty('company', '$account.company');
    tracker.track();
    return false;
    }
    (function() {
    var wsc = document.createElement('script');
    wsc.src = document.location.protocol+'//static.woopra.com/js/woopra.js';
    wsc.type = 'text/javascript';
    wsc.async = true;
    var ssc = document.getElementsByTagName('script')[0];
    ssc.parentNode.insertBefore(wsc, ssc);
    })();
    </script>
    <!-- End of Woopra -->
    <body>
        <div class="pagewrap clear">
            
            <?php include("login.php"); ?>
            <?php include("sticket.php"); ?>
            <div id="sprite5" class="none"></div>
            <!-- <a href="http://twitter.com/go_dynacom" target="_blank"><div class="twitter"></div></a> -->
            
            <!-- <div class="fullblock loginblock ">
                <div class="blockwrap login clear">
                    
                    <div class="left">
                        <div class="fb-like" data-href="https://www.facebook.com/pages/Dynamic-Communications/250966124926357" data-send="true" data-layout="button_count" data-width="250" data-show-faces="false"></div>
                    </div>
                    
                    <div class="right">
                        <ul class="loginlist">
                            <li><a href="#javascript;" id="loginbtn"><img src="images/usericon.png" height="12"> Login</a></li>
                            <li><a href="#javascript;" id="sticketbtn"><img src="images/sticketicon.png" height="12"> Service Ticket</a></li>
                            <li><a href="intranet.php" id="sticketbtn"><img src="images/intranetIcon.png" height="12"> Intranet</a></li>
                        </ul>
                    </div>
                </div>
            </div> -->
            
            <div class="fullblock header pheight" style="padding-top: 20px;">
                <div class="blockwrap clear">
                    <div class="left"><a href="index.php" style="position: relative; top: 5px;"><img src="images/logo_dynacom.svg" width="300"></a></div>
                    <div class="right"><?php include("navigation.php"); ?></div>
                </div>
            </div>
            
            <div class="fullblock sliderblockhome" style="margin-top: 78px;">
                <div class="slideconthome">
                    <div id="slides">
                        <div class="slide"><a href="#javascript;"><img src="images/banners/index/banner_index_us.jpg" align="absmiddle" width="960"></a></div>
                        <div class="slide"><a href="collaboration.php"><img src="images/banners/index/2.jpg" align="absmiddle" width="960"></a></div>
                        <div class="slide"><a href="datacenter.php"><img src="images/Main-Data-Center-Banner.jpg" align="absmiddle" width="960"></a></div>
                        <div class="slide"><a href="its.php"><img src="images/banners/index/3.jpg" align="absmiddle" width="960"></a></div>
                        <div class="slide"><a href="software.php"><img src="images/banners/index/4.jpg" align="absmiddle" width="960"></a></div>
                        <!-- <div class="slide"><a href="electronic.php"><img src="images/banners/index/5.jpg" align="absmiddle" width="960"></a></div> -->
                        <div class="slide"><a href="network.php"><img src="images/banners/index/6.jpg" align="absmiddle" width="960"></a></div>
                        <div class="slide"><a href="wifi.php"><img src="images/banners/index/7.jpg" align="absmiddle" width="960"></a></div>
                        <!-- <div class="slide"><a href="solutions.php"><img src="images/productMapBannerHome.png" align="absmiddle" width="960"></a></div> -->
                        <div class="slide"><a href="certifications.php"><img src="images/banners/index/9.jpg" align="absmiddle" width="960"></a></div>
                        <div class="slide"><a href="dynacomtv.php"><img src="images/banners/index/10.jpg" align="absmiddle" width="960"></a></div>
                    </div>
                </div>
                <div class="blockwrap thumbs">
                    <div id="menu">
                        <ul class="sliderthumbs">
                            <li class="menuItem"><a href="#javascript;"><img src="images/bgsliderthumb.png" class="thumbslider"><div class="slidedesc">12th Anniversary</div></a></li>
                            <li class="menuItem"><a href="#javascript;"><img src="images/bgsliderthumb.png" class="thumbslider"><div class="slidedesc">Collaboration Solutions</div></a></li>
                            <li class="menuItem"><a href="#javascript;"><img src="images/bgsliderthumb.png" class="thumbslider"><div class="slidedesc">Data Center Solutions</div></a></li>
                            <li class="menuItem"><a href="#javascript;"><img src="images/bgsliderthumb.png" class="thumbslider"><div class="slidedesc">ITS Solutions</div></a></li>
                            <li class="menuItem"><a href="#javascript;"><img src="images/bgsliderthumb.png" class="thumbslider"><div class="slidedesc">Software Solutions</div></a></li>
                            <!-- <li class="menuItem"><a href="#javascript;"><img src="images/bgsliderthumb.png" class="thumbslider"><div class="slidedesc">Electronic Security</div></a></li> -->
                            <li class="menuItem"><a href="#javascript;"><img src="images/bgsliderthumb.png" class="thumbslider"><div class="slidedesc">Network Security</div></a></li>
                            <li class="menuItem"><a href="#javascript;"><img src="images/bgsliderthumb.png" class="thumbslider"><div class="slidedesc">Access and Wireless</div></a></li>
                            <!-- <li class="menuItem"><a href="#javascript;"><img src="images/bgsliderthumb.png" class="thumbslider"><div class="slidedesc">Product Map</div></a></li> -->
                            <li class="menuItem"><a href="#javascript;"><img src="images/bgsliderthumb.png" class="thumbslider"><div class="slidedesc">Certifications</div></a></li>
                            <li class="menuItem"><a href="#javascript;"><img src="images/bgsliderthumb.png" class="thumbslider"><div class="slidedesc">Dynacom TV</div></a></li>
                        </ul>
                    </div>
                </div>
                
            </div>
            <div class="blockwrap sliderbar">
                
                <div class="blockwrap clear" style="padding-left:10px;">
                    <div class="left"><p>News Feed @DYNACOM: New online colaboration solution. learn more</p></div>
                    <div class="right facebook mwidthright">
                        
                    </div>
                </div>
                
            </div>