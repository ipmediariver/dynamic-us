<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<cfif IsDefined("URL.ENVIAR_MAIL") and ENVIAR_MAIL EQ 1>
    <cfmail to="desarrolloweb@godynacom.com" from="desarrolloweb@godynacom.com" type="html" subject="Pruba aplicacion">
    	Pruba de correo electronico aplicacion dynacom
    </cfmail>
    se ha enviado el correo
</cfif>

<cfform name="form1" method="post" action="prueba_mail.cfm?ENVIAR_MAIL=1">
<input type="submit" name="Enviar" value="Enviar correo" />
</cfform>

<cfquery name="ListadoTickets" datasource="Dynacom_SQL_test" username="#iSQL_UserName#" password="#iSQL_Password#">
    	SELECT DC_Service_Tickets.*,DC_Service_Customers.Company_Name 
  		FROM 	DC_Service_Tickets INNER JOIN DC_Service_Customers ON 
	   			DC_Service_Customers.Customer_number = DC_Service_Tickets.Customer_Number 
		WHERE status <> 'CLOSED'
        order by DC_Service_Tickets.Id DESC
  </cfquery>
  
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr> 
              <td width="75%"><b class="nav">TICKETS DE SERVICIO</b></td>
              <td width="25%" align="right"><span class="minitxt"><a href="contenido.cfm?cont=ADMINISTRACION_TICKETS&seccion=MENU">REGRESAR AL MENU</a></span></td>
          </tr>
        </table>
		<table width="100%" border="0" cellpadding="2" cellspacing="0">
                    <tr> 
                      <td align="center"><CF_Search_NextPrevious
					QueryRecordCount="#ListadoTickets.RecordCount#"
					FileName="contenido.cfm"
					MaxresultPages="10"
					MaxRowsAllowed="25"
					PageText=""
					LayoutNumber="2"
					Layout_Start="(&nbsp;"
                    Layout_End="&nbsp;)"
					PresetStyle="0"
					TEXTSTYLE1 ="hilight"
					FirstLastPage="numeric"
					ThisPageStyle="CustomThisPageStyle"
					Separator_mid="&nbsp;&nbsp;"
					CurrentPageWrapper_start="&nbsp;&nbsp;[&nbsp;"
					CurrentPageWrapper_end="&nbsp;]&nbsp;&nbsp;"
					ExtraURLString="cont=ADMINISTRACION_TICKETS&seccion=LISTADO_TICKETS"></td>
                    </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr bgcolor="#E2E2E2"> 
            <td width="7%" class="medium"><b>
			<cfif IsDefined("URL.Order") AND URL.Order EQ "CONSECUTIVE_DESC">
                <a href="contenido.cfm?cont=ADMINISTRACION_TICKETS&seccion=LISTADO_TICKETS&order=CONSECUTIVE_ASC"  onMouseOver="MM_displayStatusMsg('ORDENAR POR FOLIO ORDEN ASCENDENTE');return document.MM_returnValue"> Consecutivo<img src="down_black.gif" width="7" height="9" border="0" align="absmiddle"></a> 
                <cfelseif IsDefined("URL.Order") AND URL.Order EQ "CONSECUTIVE_ASC">
                <a href="contenido.cfm?cont=ADMINISTRACION_TICKETS&seccion=LISTADO_TICKETS&order=CONSECUTIVE_DESC" onMouseOver="MM_displayStatusMsg('ORDENAR POR FOLIO ORDEN DESCENDENTE');return document.MM_returnValue"> Consecutivo <img src="up_black.gif" width="7" height="9" border="0" align="absmiddle"></a> 
                <cfelse>
                <a href="contenido.cfm?cont=ADMINISTRACION_TICKETS&seccion=LISTADO_TICKETS&order=CONSECUTIVE_ASC"  onMouseOver="MM_displayStatusMsg('ORDENAR POR FOLIO ORDEN ASCENDENTE');return document.MM_returnValue"> Consecutivo</a>  
              </cfif>
			</b></td>
            <td width="8%" class="medium"><b><cfif IsDefined("URL.Order") AND URL.Order EQ "NUM_TICKET_DESC">
                <a href="contenido.cfm?cont=ADMINISTRACION_TICKETS&seccion=LISTADO_TICKETS&order=NUM_TICKET_ASC"  onMouseOver="MM_displayStatusMsg('ORDENAR POR NUMERO DE TICKET ORDEN ASCENDENTE');return document.MM_returnValue">N&uacute;mero<br>
				del Ticket <img src="down_black.gif" width="7" height="9" border="0" align="absmiddle"></a> 
                <cfelseif IsDefined("URL.Order") AND URL.Order EQ "NUM_TICKET_ASC">
                <a href="contenido.cfm?cont=ADMINISTRACION_TICKETS&seccion=LISTADO_TICKETS&order=NUM_TICKET_DESC" onMouseOver="MM_displayStatusMsg('ORDENAR POR NUMERO DE TICKET ORDEN DESCENDENTE');return document.MM_returnValue"> N&uacute;mero<br>
                del Ticket <img src="up_black.gif" width="7" height="9" border="0" align="absmiddle"></a> 
                <cfelse>
                <a href="contenido.cfm?cont=ADMINISTRACION_TICKETS&seccion=LISTADO_TICKETS&order=NUM_TICKET_ASC"  onMouseOver="MM_displayStatusMsg('ORDENAR POR NUMERO DE TICKET ORDEN ASCENDENTE');return document.MM_returnValue"> N&uacute;mero <br>
                del Ticket</a>  
              </cfif>
			</b></td>
            <td width="14%" class="medium"> <div align="left"><b><cfif IsDefined("URL.Order") AND URL.Order EQ "NUM_CLIENTE_DESC">
                <a href="contenido.cfm?cont=ADMINISTRACION_TICKETS&seccion=LISTADO_TICKETS&order=NUM_CLIENTE_ASC"  onMouseOver="MM_displayStatusMsg('ORDENAR POR NUMERO DE CLIENTE ORDEN ASCENDENTE');return document.MM_returnValue">N&uacute;mero 
                <br>
                del Cliente <img src="down_black.gif" width="7" height="9" border="0" align="absmiddle"></a> 
                <cfelseif IsDefined("URL.Order") AND URL.Order EQ "NUM_CLIENTE_ASC">
                <a href="contenido.cfm?cont=ADMINISTRACION_TICKETS&seccion=LISTADO_TICKETS&order=NUM_CLIENTE_DESC" onMouseOver="MM_displayStatusMsg('ORDENAR POR NUMERO DE CLIENTE ORDEN DESCENDENTE');return document.MM_returnValue"> N&uacute;mero 
                <br>
                del Cliente<img src="up_black.gif" width="7" height="9" border="0" align="absmiddle"></a> 
                <cfelse>
                <a href="contenido.cfm?cont=ADMINISTRACION_TICKETS&seccion=LISTADO_TICKETS&order=NUM_CLIENTE_ASC"  onMouseOver="MM_displayStatusMsg('ORDENAR POR NUMERO DE CLIENTE ORDEN ASCENDENTE');return document.MM_returnValue">N&uacute;mero 
                <br>
                del Cliente</a>  
              </cfif>
			</b></div></td>
            <td width="6%" class="medium"><b><cfif IsDefined("URL.Order") AND URL.Order EQ "SEVERITY_DESC">
                <a href="contenido.cfm?cont=ADMINISTRACION_TICKETS&seccion=LISTADO_TICKETS&order=SEVERITY_ASC"  onMouseOver="MM_displayStatusMsg('ORDENAR POR GRADO DE SEVERIDAD ORDEN ASCENDENTE');return document.MM_returnValue">Grado 
              de <br>
              Severidad <img src="down_black.gif" width="7" height="9" border="0" align="absmiddle"></a> 
                <cfelseif IsDefined("URL.Order") AND URL.Order EQ "SEVERITY_ASC">
                <a href="contenido.cfm?cont=ADMINISTRACION_TICKETS&seccion=LISTADO_TICKETS&order=SEVERITY_DESC" onMouseOver="MM_displayStatusMsg('ORDENAR POR GRADO DE SEVERIDAD ORDEN DESCENDENTE');return document.MM_returnValue"> Grado de<br>
                Severidad <img src="up_black.gif" width="7" height="9" border="0" align="absmiddle"></a> 
                <cfelse><a href="contenido.cfm?cont=ADMINISTRACION_TICKETS&seccion=LISTADO_TICKETS&order=SEVERITY_ASC"  onMouseOver="MM_displayStatusMsg('ORDENAR POR GRADO DE SEVERIDAD ORDEN ASCENDENTE');return document.MM_returnValue">Grado 
              de <br>
              Severidad</a>  
              </cfif></b></td>
            <td width="4%" align="right" class="medium">
              <div align="center"><b>Status</b></div>
            </td>
            <td width="10%" align="right" class="medium"> 
              <div align="left"><b>Contacto
                T&eacute;cnico</b> </div>
            </td>
            <td width="11%" align="right" class="medium"><div align="left"><b>Ingeniero Asignado
            </b></div></td>
            <td width="30%" align="center" class="medium"><div align="center"><b>Descripcion de la Falla<br>
            </b></div></td>
            <td width="10%" align="right" class="medium"><div align="center"><b>Fecha de 
            Ticket</b></div></td>
          </tr>
          <cfoutput query="ListadoTickets"  startrow="#URL.strt#" maxrows="#URL.show#"> 
            <tr  class="smalltxt" bgcolor="###IIF(ListadoTickets.currentrow MOD 2, DE('E1E7EC'), DE('FFFFFF'))#"> 
              <td class="smalltxt"><a href="contenido.cfm?cont=ADMINISTRACION_TICKETS&seccion=DISPLAY_TICKET&id_tk=#Ticket_Number#&id_cust=#Customer_Number#" onMouseOver="MM_displayStatusMsg('STATUS');return document.MM_returnValue">#Id#</a></td>
              <td class="smalltxt">#Ticket_Number#</td>
              <td class="smalltxt"> 
                <div align="left"> #Company_Name#</div></td>
              <td class="smalltxt">#severity#</td>
              <td align="right" class="smalltxt">
                <div align="center">#status#</div>
              </td>
              <td align="right" class="smalltxt"> 
                <div align="left">#Technical_contact#</div>
              </td>
              <td align="center" class="smalltxt">#assigned_to# 
               </td>
              <td align="center" class="smalltxt">#description# </td>
              <td align="center" class="smalltxt">#DateFormat(Date_TimeRequest,'MMM-DD-YY')# <br>
                #TimeFormat(Date_TimeRequest,'h:mm t')#</td>
            </tr>
          </cfoutput> 
        </table> 
  
  <!---<cfdump var="#ListadoTickets#">--->

</body>
</html>


