<h1>Partners</h1>
<p>
	We strongly believe that good relationships are essential on establishing 
	excellent quality services with the highest of the industry´s standards, 
	maintaining top notch customer service and support is our reason why we partner 
	with the best IT companies in the world, and here are some examples:
</p>

<div class="clear">
	<div class="left" style="width:25%;">
        <img src="images/partners-logos/avaya.jpg" width="140">
	</div>
	<div class="left" style="width:25%;">
        <img src="images/partners-logos/schneider.jpg" width="140">
	</div>
	<div class="left" style="width:25%;">
        <img src="images/partners-logos/cisco.jpg" width="140">
	</div>
	<div class="left" style="width:25%;">
        <img src="images/partners-logos/panduit.jpg" width="140">
	</div>
</div>
<div class="clear" style="margin-top:20px;">
	<div class="left" style="width:25%;">
        <img src="images/partners-logos/commscope.jpg" width="140">
	</div>
	<div class="left" style="width:25%;">
        <img src="images/partners-logos/belden.jpg" width="140">
	</div>
	<!-- <div class="left" style="width:25%;">
        <img src="images/partners-logos/tyco.jpg" width="140">
	</div> -->
	<div class="left" style="width:25%;">
        <img src="images/logos/hp.png" height="60px">
	</div>
</div>


                  