<h1>Certifications</h1>
<p>Part of why customers prefer our services instead of our competition are our certifications, as you may know IT oriented companies and solutions require certification for their implementation, this is why we are proud to present you some of our certifications.</p>

<div class="clear" style="margin-top:40px;">
<div class="clear">
<img src="images/Avaya Silver SME expert.jpg" height="50" style="margin-top:8px;" class="left mwidthright">
<img src="images/apccert.png" height="60" class="left mwidthright">
<img src="images/ciscocert.png" height="60" class="left mwidthright">
<img src="images/bicsicert.png" height="50" class="left mwidthright">
<img src="images/bbbcert.png" height="50" style="margin-top:5px;" class="left mwidthright">
<img src="images/logos/hp.png" height="50" style="margin-top:5px;" class="left">
</div>
<div class="clear" style="margin-top:15px;">
<img src="images/LOGOS/Commscope_Logo.JPG" height="30" style="margin-top:5px;" class="left mwidthright">
<img src="images/LOGOS/panduit-logo.jpg" height="30" style="margin-top:5px;" class="left">
</div>
</div>
