<?php include("headertwo.php"); ?>
<div class="fullblock sliderblock">
	<div class="blockwrap aboutimages">
		<img src="images/overview.jpg" align="absmiddle" class="firstimage">
		<img src="images/overview.jpg" align="absmiddle" alt="overview" style="display:none">
		<img src="images/history.jpg" align="absmiddle" alt="history" style="display:none">
		<img src="images/Presence_banner.jpg" align="absmiddle" alt="offices" style="display:none">
		<img src="images/partners.jpg" align="absmiddle" alt="partners" style="display:none">
		<img src="images/certifications.jpg" align="absmiddle" alt="certifications" style="display:none">
	</div>
</div>
<div class="blockwrap sliderbarblue">
	<div class="blockwrap clear pwidthleft">
		<?php include("navs/aboutnav.php"); ?>
	</div>
</div>
<div class="fullblock container">
	<div class="fullblock">
		<div class="blockwrap clear pheight">
			<div class="left article">
				<div id="contents">
					<h1>Overview</h1>
					<p>For now, more than 12 years, <b>Dynamic Communications</b> has been an industry leader in Seamless IP Telephony Applications, Data Networks and Structured Cabling Solutions. Our customers range from Small Businesses to Large Multinational Companies, across a variety of Vertical Markets.</p>

					<p>We have the ability to tackle any size project in for our Multinational Customers who demand a Single Point of Contact for their solution design, purchase and deployment.</p>

					<p>Our Solution Portfolio has increased with time and we are proud that our products and Services Offering now includes Cloud Based Services and Software.</p>
				</div>
			</div>
			<?php include("aside.php"); ?>
		</div>
	</div>
</div>
<?php include("footer.php"); ?>
</div>
</body>
</html>