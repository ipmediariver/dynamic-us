<?php include("headertwo.php"); ?>

<div class="fullblock sliderblock">
	<div class="blockwrap clear" style="position:relative; height:289px;">
		<img src="images/dynacomtv1.jpg" align="absmiddle" class="firstimage">
		<div class="theVideoBox">
			<h3>Featured Video!</h3>
			<h4>Dynamic Communications Corporate Video</h4>
			<a href="#theVideoPlay" class="theVideoBoxLink fancybox" alt="https://www.youtube.com/embed/6hnDgRalHEk?rel=0">Click here to watch</a>
		</div>
	</div>
</div>
<div class="fullblock container">
	<div class="fullblock">
		<div class="blockwrap clear pheight">
			<div id="theVideoPlay" class="none">
			</div>
			<div class="videoBox">
				<div style="width:310px;" class="left">
					<a href="#theVideoPlay" class="videoBoxLink fancybox" alt="https://www.youtube.com/embed/4JUAugaVYXQ?rel=0">
					    <img src="images/videoThumb/1.jpg" align="absmiddle" style="width:100%;">
					    <img src="images/playIcon.png" class="absolute playIcon" style="opacity: 0.5; top: 50%; margin-top: -30px; left: 50%; margin-left: -30px;">
					</a>
				</div>
				<div style="width:650px;" class="right">
					<div style="padding-left:20px;">
						<h2 style="color:#2a648e;">How Cool Is Video Conferencing?</h2>
                        Video conferencing was often considered a technology of the future, but it's real, and it's here 
                        today. IT often worried about the costs associated with deploying and managing video networks
					</div>
				</div>
			</div>

			<div class="videoBox">
				<div style="width:310px;" class="left">
					<a href="#theVideoPlay" class="videoBoxLink fancybox" alt="https://www.youtube.com/embed/6HSW59ny4z8?rel=0">
					    <img src="images/videoThumb/2.jpg" align="absmiddle" style="width:100%;">
					    <img src="images/playIcon.png" class="absolute playIcon" style="opacity: 0.5; top: 50%; margin-top: -30px; left: 50%; margin-left: -30px;">
					</a>
				</div>
				<div style="width:650px;" class="right">
					<div style="padding-left:20px;">
						<h2 style="color:#2a648e;">eVident - Monitoring and Improving Your Video Network</h2>
                        Avaya's eVident allows enterprise-level customers to proactively ensure network readiness 
                        before and after voice and video services are deployed.
					</div>
				</div>
			</div>

			<!-- <div class="videoBox">
				<div style="width:310px;" class="left">
					<a href="#theVideoPlay" class="videoBoxLink fancybox" alt="http://www.youtube.com/embed/wXkfrBJqVcQ?rel=0">
					    <img src="images/videoThumb/3.jpg" align="absmiddle" style="width:100%;">
					    <img src="images/playIcon.png" class="absolute playIcon" style="opacity: 0.5; top: 50%; margin-top: -30px; left: 50%; margin-left: -30px;">
					</a>
				</div>
				<div style="width:650px;" class="right">
					<div style="padding-left:20px;">
						<h2 style="color:#2a648e;">Cognitive Computing: 5 Future Technology Innovations from IBM</h2>
                        The goal of cognitive computing is to get a computer to behave, think and interact the way 
                        humans do. In 5 years, machines will emulate.
					</div>
				</div>
			</div> -->

			<!-- <div class="videoBox">
				<div style="width:310px;" class="left">
					<a href="#theVideoPlay" class="videoBoxLink fancybox" alt="http://www.youtube.com/embed/39jtNUGgmd4?rel=0">
					    <img src="images/videoThumb/4.jpg" align="absmiddle" style="width:100%;">
					    <img src="images/playIcon.png" class="absolute playIcon" style="opacity: 0.5; top: 50%; margin-top: -30px; left: 50%; margin-left: -30px;">
					</a>
				</div>
				<div style="width:650px;" class="right">
					<div style="padding-left:20px;">
						<h2 style="color:#2a648e;">IBM Centennial Film: 100 X 100</h2>
                         The film chronology flows from the oldest person to the youngest, offering a whirlwind history 
                         of the company and culminating with its prospects for the future
					</div>
				</div>
			</div> -->

			<div class="videoBox">
				<div style="width:310px;" class="left">
					<a href="#theVideoPlay" class="videoBoxLink fancybox" alt="http://www.youtube.com/embed/PlxWYtetGis?rel=0">
					    <img src="images/videoThumb/5.jpg" align="absmiddle" style="width:100%;">
					    <img src="images/playIcon.png" class="absolute playIcon" style="opacity: 0.5; top: 50%; margin-top: -30px; left: 50%; margin-left: -30px;">
					</a>
				</div>
				<div style="width:650px;" class="right">
					<div style="padding-left:20px;">
						<h2 style="color:#2a648e;">Secure Mobile Access: Is Your Organization Ready?</h2>
                         Protect your data, enable better collaboration and grow business. Are you prepared 
                         to empower mobile workers while minimizing company risk? Watch the video, then take the test:
					</div>
				</div>
			</div>

			<div class="videoBox">
				<div style="width:310px;" class="left">
					<a href="#theVideoPlay" class="videoBoxLink fancybox" alt="http://www.youtube.com/embed/M578lU2TGeI?rel=0">
					    <img src="images/videoThumb/6.jpg" align="absmiddle" style="width:100%;">
					    <img src="images/playIcon.png" class="absolute playIcon" style="opacity: 0.5; top: 50%; margin-top: -30px; left: 50%; margin-left: -30px;">
					</a>
				</div>
				<div style="width:650px;" class="right">
					<div style="padding-left:20px;">
						<h2 style="color:#2a648e;">Internet of Everything Economy HD</h2>
                         Discover the value of connections when people, process, data and things 
                         converge on the Internet of Everything
					</div>
				</div>
			</div>

			<div class="videoBox">
				<div style="width:310px;" class="left">
					<a href="#theVideoPlay" class="videoBoxLink fancybox" alt="http://www.youtube.com/embed/4_dHQn4mdAg?rel=0">
					    <img src="images/videoThumb/7.jpg" align="absmiddle" style="width:100%;">
					    <img src="images/playIcon.png" class="absolute playIcon" style="opacity: 0.5; top: 50%; margin-top: -30px; left: 50%; margin-left: -30px;">
					</a>
				</div>
				<div style="width:650px;" class="right">
					<div style="padding-left:20px;">
						<h2 style="color:#2a648e;">Considerations for Building or Expanding Data Centers</h2>
                         The skyrocketing demand for storage and networking capabilities are putting in-house 
                         and hosted data centers under greater pressure than ever before
					</div>
				</div>
			</div>

			<div class="videoBox">
				<div style="width:310px;" class="left">
					<a href="#theVideoPlay" class="videoBoxLink fancybox" alt="http://www.youtube.com/embed/xZUpRL6JDeU?rel=0">
					    <img src="images/videoThumb/8.jpg" align="absmiddle" style="width:100%;">
					    <img src="images/playIcon.png" class="absolute playIcon" style="opacity: 0.5; top: 50%; margin-top: -30px; left: 50%; margin-left: -30px;">
					</a>
				</div>
				<div style="width:650px;" class="right">
					<div style="padding-left:20px;">
						<h2 style="color:#2a648e;">SYSTIMAX® InstaPATCH® QUATTRO Introduction Video</h2>
                         InstaPATCH QUATTRO is a 10GBASE-T and 1GBASE-T cassette and trunk based connectivity solution 
                         that combines high-performance with plug-and-play simplicity for fast data center deployment 
                         and easy modification as your network needs evolve. - for more information:
					</div>
				</div>
			</div>

			<div class="videoBox">
				<div style="width:310px;" class="left">
					<a href="#theVideoPlay" class="videoBoxLink fancybox" alt="http://www.youtube.com/embed/pRTi33fcvlY?rel=0">
					    <img src="images/videoThumb/9.jpg" align="absmiddle" style="width:100%;">
					    <img src="images/playIcon.png" class="absolute playIcon" style="opacity: 0.5; top: 50%; margin-top: -30px; left: 50%; margin-left: -30px;">
					</a>
				</div>
				<div style="width:650px;" class="right">
					<div style="padding-left:20px;">
						<h2 style="color:#2a648e;">StruxureWare for Data Centers - Discovering an Opportunity</h2>
                         Balance - the great data center challenge. On one side, there is availability, 
                         and on the other is efficiency.
					</div>
				</div>
			</div>

			<div class="videoBox">
				<div style="width:310px;" class="left">
					<a href="#theVideoPlay" class="videoBoxLink fancybox" alt="http://www.youtube.com/embed/Vu7FJvL8ArA?rel=0">
					    <img src="images/videoThumb/10.jpg" align="absmiddle" style="width:100%;">
					    <img src="images/playIcon.png" class="absolute playIcon" style="opacity: 0.5; top: 50%; margin-top: -30px; left: 50%; margin-left: -30px;">
					</a>
				</div>
				<div style="width:650px;" class="right">
					<div style="padding-left:20px;">
						<h2 style="color:#2a648e;">Advanced Distribution Management System</h2>
                         Turn data into actionable business intelligence that can increase efficiencies 
                         and support decisions across your entire enterprise. 
					</div>
				</div>
			</div>

			<div class="videoBox">
				<div style="width:310px;" class="left">
					<a href="#theVideoPlay" class="videoBoxLink fancybox" alt="http://www.youtube.com/embed/mXYjjXyRnMM?rel=0">
					    <img src="images/videoThumb/11.jpg" align="absmiddle" style="width:100%;">
					    <img src="images/playIcon.png" class="absolute playIcon" style="opacity: 0.5; top: 50%; margin-top: -30px; left: 50%; margin-left: -30px;">
					</a>
				</div>
				<div style="width:650px;" class="right">
					<div style="padding-left:20px;">
						<h2 style="color:#2a648e;">Plantronics Brings Communication All TOGETHER</h2>
                         With smart call routing, one-word voice command, and precision audio, the Voyager 
                         Legend headset brings people, devices and every aspect of your life together. 
					</div>
				</div>
			</div>

			<div class="videoBox">
				<div style="width:310px;" class="left">
					<a href="#theVideoPlay" class="videoBoxLink fancybox" alt="http://www.youtube.com/embed/KsNGwfxBYgE?rel=0">
					    <img src="images/videoThumb/12.jpg" align="absmiddle" style="width:100%;">
					    <img src="images/playIcon.png" class="absolute playIcon" style="opacity: 0.5; top: 50%; margin-top: -30px; left: 50%; margin-left: -30px;">
					</a>
				</div>
				<div style="width:650px;" class="right">
					<div style="padding-left:20px;">
						<h2 style="color:#2a648e;">Plantronics Voyager Legend: Voice Command</h2>
                         With audio caller ID and one-word voice commands on the Plantronics Voyager Legend, 
                         there is no need to touch your phone to be productive.
					</div>
				</div>
			</div>

			<div class="videoBox">
				<div style="width:310px;" class="left">
					<a href="#theVideoPlay" class="videoBoxLink fancybox" alt="http://www.youtube.com/embed/A20Pf9l1VHI?rel=0">
					    <img src="images/videoThumb/13.jpg" align="absmiddle" style="width:100%;">
					    <img src="images/playIcon.png" class="absolute playIcon" style="opacity: 0.5; top: 50%; margin-top: -30px; left: 50%; margin-left: -30px;">
					</a>
				</div>
				<div style="width:650px;" class="right">
					<div style="padding-left:20px;">
						<h2 style="color:#2a648e;">Plantronics Voyager Legend: Precision Audio</h2>
                         The Plantronics Voyager Legend precision audio eliminates background noise, 
                         ensuring a professional conversation in any environment.
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<?php include("footer.php"); ?>

</div>
</body>
</html>